extern crate ncurses;

use ncurses::*;

fn main() {
    initscr();
    keypad(stdscr(), true);
    noecho();
    cbreak();

    curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);
    let mut ch: i32;
    let mut key_name: &str;
    let (mut max_x, mut max_y) = (0,0);
    getmaxyx(stdscr(), &mut max_x, &mut max_y);

    print_centered(&max_x, &max_y, "Press any key, F2 to quit");

    loop {
        ch = getch();

        match ch {
            KEY_UP => key_name = "UP",
            KEY_DOWN => key_name = "DOWN",
            KEY_RIGHT => key_name = "RIGHT",
            KEY_LEFT => key_name = "LEFT",
            _ => key_name = "other key"
        }

        print_centered(&max_x, &max_y, &format!("Got {}", key_name));

        if(ch == KEY_F2){
            break;
        }
    }

    endwin();
}

fn print_centered(max_x: &i32, max_y: &i32, msg: &str) {
    clear();
    let pos_y : i32 = max_y/2 - ((msg.len()/2) as i32);
    mvprintw(max_x/2, pos_y, &msg);
    refresh();
}
